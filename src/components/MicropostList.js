import React, {Component} from 'react'
import Micropost from './Micropost'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
class MicropostList extends Component {
  render () {
    if (this.props.micropostQuery && this.props.micropostQuery.loading) {
      return <div>Loading</div>
    }

    // 2
    if (this.props.micropostQuery && this.props.micropostQuery.error) {
      console.log(this.props.micropostQuery.error)
      return <div>Error</div>
    }
    const micropostsToRender = this.props.micropostQuery.allMicroposts
     // 3

    return (
      <div className='background-gray'>
        <h1>All posts made by the users.</h1><hr />
        <div id='microposts'>
          { micropostsToRender.map((micropost) => (
            <div><Micropost key={micropost.id} {...micropost} /><hr /></div>
          ))}
        </div>
      </div>
    )
  }
}

const MICRPOPOST_QUERY = gql`
query micropostQuery {
    allMicroposts {
      content
      id
      user_id {
        name
      }
    }
  }
`

export default graphql(MICRPOPOST_QUERY, { name: 'micropostQuery' })(MicropostList)
