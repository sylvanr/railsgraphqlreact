import React, { Component } from 'react'
import { AUTH_TOKEN } from '../constants'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

class Login extends Component {
  state = {
    login: true, // switch between Login and SignUp
    email: '',
    password: '',
    name: '',
  }

  render() {
    return (
      <div className='background-gray'>
        <h4 className="mv3">{this.state.login ? 'Log in to see all the posts!' : 'Sign Up'}</h4>
        <div className="flex flex-column">
          {!this.state.login && (
            <input
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })}
              type="text"
              placeholder="Name"
            />
          )}
          <input
            value={this.state.email}
            onChange={e => this.setState({ email: e.target.value })}
            type="text"
            placeholder="Email address"
          />
          <input
            value={this.state.password}
            onChange={e => this.setState({ password: e.target.value })}
            type="password"
            placeholder="Password"
          />
        </div>
        <div className="flex mt3">
          <div className="pointer mr2 button" onClick={() => this._confirm()}>
            {this.state.login ? 'login' : 'create account'}
          </div>
          <div
            className="pointer button"
            onClick={() => this.setState({ login: !this.state.login })}
          >
            {this.state.login
              ? 'need to create an account?'
              : 'already have an account?'}
          </div>
        </div>
        <div id="errormessage"></div>
      </div>
    )
  }

  _confirm = async () => {
    const {name, email, password} = this.state
    if (this.state.login) {
      try {
        const result = await this.props.loginMutation({
          variables: {
            email,
            password
          }
        })
        const {token} = result.data.signinUser
        this._saveUserData(token)
        this.props.history.push('/posts')
      }
      catch(err) {
        document.getElementById("errormessage").innerHTML = "This account does not exist."
        console.log(err);
      }
    } else {
      try {
        const result = await this.props.signupMutation({
          variables: {
            name, email, password
          }
        })
        const {token} = result.data.createUser
        this._saveUserData(token)
        this.props.history.push(`/posts`)
      }
      catch (err) {
        document.getElementById("errormessage").innerHTML = err.toString().substring(37)
      }
    }
  }
  _saveUserData = token => {
    localStorage.setItem(AUTH_TOKEN, token)
  }

}

const SIGNUP_MUTATION = gql`
  mutation SignupMutation($email: String!, $password: String!, $name: String!) {
    createUser(authProvider:{email:{email: $email, password: $password}}, name: $name) {
      id
    }
  }
`

const LOGIN_MUTATION = gql`
  mutation LoginMutation($email: String!, $password: String!) {
    signinUser(email:{email: $email, password: $password}) {
      token
    }
  }
`

export default compose(
  graphql(SIGNUP_MUTATION, { name: 'signupMutation' }),
  graphql(LOGIN_MUTATION, { name: 'loginMutation' }),
)(Login)
