import React, { Component } from 'react'
import '../styles/App.css'
import MicropostList from './MicropostList'
import Login from './Login'
import { Switch, Route } from 'react-router-dom'

class App extends Component {
  render () {
    return (
      <div className='center w85'>
        <div className='ph3 pv1 background-gray'>
          <Switch>
            <Route exact path='/' component={Login} />
            <Route exact path='/posts' component={MicropostList} />
          </Switch>
        </div>
      </div>
    )
  }
}

export default App
