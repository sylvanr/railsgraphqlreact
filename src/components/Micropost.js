import React from 'react'

// class Micropost extends Component {
//   render () {
//     console.log(this.props);
//     const {
//       id,
//       description
//     } = this.props;
//
//     return (
//       <div>
//         {id}, {description} <br />
//         {/*Posted by {this.props.micropost.user_id.name}*/}
//       </div>
//     )
//   }
// }

const Micropost = ({ id, content, user_id, ...other }) => (
  <div>
    <div {...other}>{id}. {content} <br />Posted by: {user_id.name} </div>
  </div>
)

export default Micropost
