import React from 'react'
import ReactDOM from 'react-dom'
import './styles/index.css'
import App from './components/App'
import registerServiceWorker from './registerServiceWorker'
import { BrowserRouter } from 'react-router-dom'

// import required apollo
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

// make a link to the place where the graphql server is running
const httpLink = new HttpLink({ uri: 'http://localhost:4000/graphql' })

// instantiate apollo client to the cache
const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
})

// render the app with ApolloProvider as a wrapper, which gets passed as a prop
ReactDOM.render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </BrowserRouter>
  , document.getElementById('root')
)
registerServiceWorker()
