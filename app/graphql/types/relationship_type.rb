#define new gql type

Types::RelationshipType = GraphQL::ObjectType.define do
  # the type name
  name 'Relationship'

  # has the fields
  field :id, !types.ID
  field :follower_id, !types.String
  field :followed_id, !types.String
end
