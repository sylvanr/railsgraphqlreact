#define new gql type

Types::UserType = GraphQL::ObjectType.define do
  # the type name
  name 'User'

  # has the fields
  field :id, !types.ID
  field :name, !types.String
  field :email, !types.String
  #field :remember_digest, !types.String
  #field :admin, !types.Boolean
end
