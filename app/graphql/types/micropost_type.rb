#define new gql type

Types::MicropostType = GraphQL::ObjectType.define do
  # the type name
  name 'Micropost'

  # has the fields
  field :id, !types.ID
  field :content, !types.String
  field :user_id, -> { Types::UserType }, property: :user
end
